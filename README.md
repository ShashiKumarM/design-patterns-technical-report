
# Introduction 

From Wikipedia
> In software engineering, a software design pattern is a general, reusable solution to a commonly occurring problem within a given context in software design. It is not a finished design that can be transformed directly into source or machine code. Rather, it is a description or template for how to solve a problem that can be used in many different situations. Design patterns are formalized best practices that the programmer can use to solve common problems when designing an application or system.


Design Patterns are common, reusable solutions to recurring problems. They are guidelines on how to tackle the problems.

# Types of design patterns

The concept of design patterns has been around in software engineering industry since the very beginning, but were not formalized. The book Design Patterns: Elements Of Reusable Object-Oriented Software written by Erich Gamma, Richard Helm, Ralph Johnson and John Vlissides, the famous Gang of Four were instrumental in formalizing the concept of design patterns.

There are 23 design patters in the book.

## Creational

- Constructor
- Factory
- Prototype
- Singleton

## Structural

- Adapter
- Composite
- Decorator
- Facade
- Flyweight
- Proxy

## Behavioral

- Chain of Responsibility
- Command
- Iterator
- Mediator
- Observer
- State
- Strategy
- Template

# Explain 5 design patterns

## 1. Singleton

From Wikipedia
> In software engineering, the singleton pattern is a software design pattern that restricts the instantiation of a class to one "single" instance. This is useful when exactly one object is needed to coordinate actions across the system. The term comes from the mathematical concept of a singleton.

Singleton is a creational design pattern where only a single instance of an class is created, and this instance is shared between different parts of the application. All the resources contained in the singleton instance of the class is shared between the parts of the application.

Code example :

```
class Counter{
        constructor() {
                if(Counter.instance == null){
                        this.counter = 0;
                        Counter.instance = this;
                }
                return Counter.instance;
        }

        incrementCounter() {
                this.counter += 1;
        }

        displayCounter() {
                console.log("The count is : ", this.counter);
        }
}

let counter = new Counter();
Object.freeze(counter);

export default counter;

```

Here Counter is a class using which a single object is created. In the class constructor a check is made to find if an instance of the class is created, if an instance has already created, it returns the instance. This prevents the constructor from creating multiple instances.
The object properties are locked using the Object.freeze() method so that properties cannot be removed or added. The created object then can imported and used in other files.

## 2. Factory

Factory

From Wikipedia
> In class-based programming, the factory method pattern is a creational pattern that uses factory methods to deal with the problem of creating objects without having to specify the exact class of the object that will be created. This is done by creating objects by calling a factory method—either specified in an interface and implemented by child classes, or implemented in a base class and optionally overridden by derived classes—rather than by calling a constructor.

Factory is a creational design pattern in which a class delegates the responsibility of object instantiation to its subclasses. This allows us to create object without having to specify the exact class of the object.

Code Example :

```
class IceCream {

    constructor() {
        this.makeIceCream = function (type) {
            let iceCream;
            if(type === 'vanilla')
                iceCream = new Vanilla();
            else if(type === 'chocolate')
                iceCream = new Chocolate();

            iceCream.describe = function() {
                return `This is a ${this.type} ice cream.`;
            }
            return iceCream;
        }
    }
}

class Vanilla() {
    constructor () {
        this._type = 'Vanilla';
        this.bought = function () {
            return 'You bought a Vanilla ice cream';
        }
    }
}

class Chocolate() {
    constructor () {
        this._type = 'Chocolate';
        this.bought = function () {
            return 'You bought a Chocolate ice cream';
        }
    }
}

const iceCream = new IceCream();
const vanillaIceCream = iceCream.makeIceCream('vanilla');
console.log(vanillaIceCream.describe());
console.log(vanillaIceCream.bought());

```

Here the class IceCream delegates the responsibility of creating the objects to the classes Vanilla and Chocolate.

## 3. Decorator

From Wikipedia
> In object-oriented programming, the decorator pattern is a design pattern that allows behavior to be added to an individual object, dynamically, without affecting the behavior of other objects from the same class. The decorator pattern is often useful for adhering to the Single Responsibility Principle, as it allows functionality to be divided between classes with unique areas of concern. The decorator pattern is structurally nearly identical to the chain of responsibility pattern, the difference being that in a chain of responsibility, exactly one of the classes handles the request, while for the decorator, all classes handle the request.

Decorator is a structural design pattern that allows functionalities and behavior to be added to existing class dynamically without affecting the behavior of the object created using the same class.

Code Example :

```
class Album {
    constructor (artist, albumName, price) {
        this._artist = artist;
        this._albumName = albumName;
        this._price = price;
    }

    displayDetails () {
        return `${this._albumName} by ${this._artist} - price ${this.price}`
    }
}

function topTen(album) {
    album.isTopTen = true;
    album.displayMessage = function () {
        return `${album.displayDetails()}`
    }
}

const newAlbum = topTen(new Album('Artist A', 'Album B', '99'));
console.log(newAlbum.displayMessage()) // Album B by Artist A - price 99
```

## 4. Facade

From Wikipedia
> The facade pattern (also spelled façade) is a software-design pattern commonly used in object-oriented programming. Analogous to a facade in architecture, a facade is an object that serves as a front-facing interface masking more complex underlying or structural code.

Facade is a structural design patterns which is used to provide a simpler, easy to use interface that shields away the actual complex subsystem. Example of facade pattern are APIs where the complex code of the APIs are hidden away from the user and only small set of simple interface are available for the user to interact.

Code Example :

Fetch API Call

```
function fetchData() {
    return fetch('https://random.api.com/data?paramOne=valueOne', {
        method : "GET,
        headers : {'Content-Type' : 'application/json'}
    }).then(data => data.json)
}

fetchData()
.then(resultData => {
    console.log(resultData);
})
`

This can be simplified using a facade for the fetch calls

`
function fetchData() {
    return fetchFacade('https://random.api.com/data', {
        paramOne : valueOne
    });
}

function fetchFacade(url, params) {

    const queryString = Object.entries(params).map(param => {
        return `${param[0]}+${param[1]}`;
    }).join('&');

    return fetch(`${url}?${queryString}`, {
        method : "GET",
        headers : {'Content-Type' : 'application/json'}
    }).then(data => data.json);
}

fetchData()
.then(resultData => {
    console.log(resultData);
})

```

Here the fetch call can be replaced by a function that takes in a url and parameters. This prevents the need to write multiple fetch calls and it makes it easier to change the code in the future.

## 5. Mediator

From Wikipedia
> In software engineering, the mediator pattern defines an object that encapsulates how a set of objects interact. This pattern is considered to be a behavioral pattern due to the way it can alter the program's running behavior.

Mediator is a behavioral design pattern that encapsulates how a set of objects interact with each other. It provides an central authority which is a mediator object over the group of objects. The object in the set no longer communicate directly, but instead communicate through te mediator object.

Code Example :

```
class TrafficTower {
  constructor() {
    this._airplanes = [];
  }

  register(airplane) {
    this._airplanes.push(airplane);
    airplane.register(this);
  }

  requestCoordinates(airplane) {
    return this._airplanes.filter(plane => airplane !== plane).map(plane => plane.coordinates);
  }
}

class Airplane {
  constructor(coordinates) {
    this.coordinates = coordinates;
    this.trafficTower = null;
  }

  register(trafficTower) {
    this.trafficTower = trafficTower;
  }

  requestCoordinates() {
    if (this.trafficTower) return this.trafficTower.requestCoordinates(this);
    return null;
  }
}

// usage
const tower = new TrafficTower();

const airplanes = [new Airplane(10), new Airplane(20), new Airplane(30)];
airplanes.forEach(airplane => {
  tower.register(airplane);
});

console.log(airplanes.map(airplane => airplane.requestCoordinates()));
// [[20, 30], [10, 30], [10, 20]]
```

[code reference](https://able.bio/drenther/javascript-design-patterns--89mv2af#mediator-pattern)

# References

- JavaScript Design Patterns https://able.bio/drenther/javascript-design-patterns--89mv2af
- Web Dev Simplified Design Patterns series  :
    - Facade    - https://youtu.be/fHPa5xzbpaA
    - Singleton - https://youtu.be/sJ-c3BA-Ypo
- Wikipedia
- https://github.com/sohamkamani/javascript-design-patterns-for-humans